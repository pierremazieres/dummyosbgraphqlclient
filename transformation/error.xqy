xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace com = "dummyOsbGraphQLClientCommon";
(:: import schema at "../schema/common/common.xsd" ::)
declare namespace dum = "dummyOsbGraphQLClient";
(:: import schema at "../schema/common/error.xsd" ::)
declare namespace soap = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element() external;

declare function local:func($body as element()) as element(soap:Fault) {
  <soap:Fault>
    <dum:errorsList>
      {for $errors in $body/*/com:errors
      return <com:errors>{fn:string($errors)}</com:errors>}
    </dum:errorsList>
  </soap:Fault>
};

local:func($body)
